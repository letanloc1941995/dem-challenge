package com.locle.test.challenge1610.di.module

import com.locle.test.challenge1610.di.provider.FeedProvider
import com.locle.test.challenge1610.presentation.feedDetail.FeedDetailActivity
import com.locle.test.challenge1610.presentation.feedlist.FeedListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule


@Module(includes = [AndroidSupportInjectionModule::class])
interface ActivityModule {

//    @ContributesAndroidInjector(modules = [FeedProvider::class])
//    fun mainActivityInjector(): MainActivity

    @ContributesAndroidInjector
    fun feedDetailActivityInjector(): FeedDetailActivity

    @ContributesAndroidInjector
    fun feedListActivityInjector(): FeedListActivity
}