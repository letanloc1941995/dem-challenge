package com.locle.test.challenge1610.presentation.feedDetail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.locle.test.challenge1610.R
import com.locle.test.challenge1610.domain.model.ArticlesItem
import com.locle.test.challenge1610.presentation.feedlist.FeedListViewModel
import dagger.android.DaggerActivity
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_feed_detail.*
import kotlinx.android.synthetic.main.item_feed.view.*
import javax.inject.Inject

class FeedDetailActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: FeedDetailViewModel by lazy {
        ViewModelProvider(this, viewModelFactory)
            .get(FeedDetailViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed_detail)
        setupActionbar()
        var data = intent.getParcelableExtra<ArticlesItem>("data")

        viewModel.articlesResevedLiveData.observe(this, Observer {
            it?.let {
                initData(it)
            }
        })
        viewModel.setFeeData(data)
    }

    private fun initData(articlesItem: ArticlesItem) {

        supportActionBar?.apply { title = articlesItem.title }

        Glide.with(this)
            .load(articlesItem.image)
            .centerCrop()
            .circleCrop()
            .placeholder(R.mipmap.ic_launcher)
            .into(image_view)

        txt_content.apply { text = articlesItem.detail }
    }

    private fun setupActionbar() {
        setSupportActionBar(tool_bar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
