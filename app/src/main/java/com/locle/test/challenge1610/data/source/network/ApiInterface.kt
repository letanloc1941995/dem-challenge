package com.locle.test.challenge1610.data.source.network

import com.locle.test.challenge1610.domain.model.FeedResponse
import io.reactivex.Single
import retrofit2.http.GET

interface ApiInterface {
    @GET("/example-feed/feed.json")
    fun invokeFeedList(): Single<FeedResponse>
}