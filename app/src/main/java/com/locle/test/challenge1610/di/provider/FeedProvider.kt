package com.locle.test.challenge1610.di.provider

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule


@Module(includes = [AndroidSupportInjectionModule::class])
abstract class FeedProvider {
//    @ContributesAndroidInjector
//    abstract fun provideFeedListFragment(): FeedListFragment

}