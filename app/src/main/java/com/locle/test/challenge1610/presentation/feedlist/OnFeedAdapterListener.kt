package com.locle.test.challenge1610.presentation.feedlist

import com.locle.test.challenge1610.domain.model.ArticlesItem

interface OnFeedAdapterListener {
    fun onClick(data:ArticlesItem,pos:Int)
}