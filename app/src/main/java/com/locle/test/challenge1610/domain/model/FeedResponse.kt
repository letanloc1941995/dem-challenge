package com.locle.test.challenge1610.domain.model

import com.squareup.moshi.Json

data class FeedResponse(
    @Json(name = "articles")
    val articles: List<ArticlesItem> = ArrayList()
)
