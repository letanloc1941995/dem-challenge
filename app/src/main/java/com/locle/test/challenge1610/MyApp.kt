package com.locle.test.challenge1610

import android.app.Activity
import android.app.Application
import android.content.Context
import com.facebook.stetho.Stetho
import com.locle.test.challenge1610.di.component.DaggerAppComponent
import com.locle.test.challenge1610.di.module.NetworkModule
import dagger.android.AndroidInjector

import dagger.android.DispatchingAndroidInjector

import dagger.android.HasAndroidInjector
import javax.inject.Inject

class MyApp : Application(), HasAndroidInjector {

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> {
        return activityDispatchingAndroidInjector;
    }

    override fun onCreate() {
        super.onCreate()
// dedug tool
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this);
        }
        return DaggerAppComponent.builder()
            .application(this)
            .build()
            .inject(this)
    }


//


}

