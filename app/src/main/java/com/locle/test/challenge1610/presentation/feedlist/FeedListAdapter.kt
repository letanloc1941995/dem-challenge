package com.locle.test.challenge1610.presentation.feedlist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.constraintlayout.widget.Guideline
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.locle.test.challenge1610.R
import com.locle.test.challenge1610.domain.model.ArticlesItem
import com.locle.test.challenge1610.domain.model.FeedResponse
import kotlinx.android.synthetic.main.item_feed.view.*
import java.util.ArrayList


class FeedListAdapter constructor(val mListener: OnFeedAdapterListener) :
    RecyclerView.Adapter<FeedListAdapter.ViewHolder>() {
    private val datas: MutableList<ArticlesItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_feed, parent, false);
        return ViewHolder(view)
    }

    fun addData(list: List<ArticlesItem>) {
        datas.clear()
        this.datas.addAll(list)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setupData(datas[position], position)
    }

    inner class ViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun setupData(data: ArticlesItem, position: Int) {

            itemView.txt_title?.apply { text = data.title }
            itemView.txt_description.apply { text = data.description }

            Glide.with(itemView.context)
                .load(data.image)
                .centerCrop()
                .circleCrop()
                .placeholder(R.mipmap.ic_launcher)
                .into(itemView.img_item);

            itemView.setOnClickListener {
                mListener.onClick(data, position)
            }
        }

    }
}