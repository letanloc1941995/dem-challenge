package com.locle.test.challenge1610.domain.usecase.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


/**
 * This class is extended by SingleUseCase classes
 * to use common methods & fields
 **/

abstract class UserCase {

    protected var lastDisposable: Disposable? = null
    protected val compositeDisposable by lazy { CompositeDisposable() }

    fun disposeLast() {
        lastDisposable?.let {
            if (!it.isDisposed) {
                it.dispose()
            }
        }
    }

    // destroy composite
    fun dispose() {
        compositeDisposable.clear()
    }

}