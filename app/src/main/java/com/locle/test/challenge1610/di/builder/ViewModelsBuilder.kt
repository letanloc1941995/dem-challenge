package com.locle.test.challenge1610.di.builder

import androidx.lifecycle.ViewModel
import com.locle.test.challenge1610.presentation.feedDetail.FeedDetailViewModel
import com.locle.test.challenge1610.presentation.feedlist.FeedListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelsBuilder {

    @Binds
    @IntoMap
    @ViewModelKey(FeedListViewModel::class)
    abstract fun bindFeedListViewModel(feedListViewModel: FeedListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FeedDetailViewModel::class)
    abstract fun bindFeedDetailViewModel(feedDetailViewModel: FeedDetailViewModel): ViewModel
}