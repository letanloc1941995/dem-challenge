package com.locle.test.challenge1610.domain.repository

import com.locle.test.challenge1610.domain.model.FeedResponse
import io.reactivex.Single

interface FeedListRepository {
    fun invokeFeedList(): Single<FeedResponse>
}
