package com.locle.test.challenge1610.presentation.feedDetail

import androidx.lifecycle.MutableLiveData
import com.locle.test.challenge1610.domain.model.ArticlesItem
import com.locle.test.challenge1610.domain.model.FeedResponse
import com.locle.test.challenge1610.domain.usecase.GetFeedListUserCase
import com.locle.test.challenge1610.presentation.base.BaseViewModel
import javax.inject.Inject


class FeedDetailViewModel @Inject constructor() :
    BaseViewModel() {
    val articlesResevedLiveData: MutableLiveData<ArticlesItem> by lazy { MutableLiveData<ArticlesItem>() }
    fun setFeeData(item: ArticlesItem) = run { articlesResevedLiveData.value = item }
    val articlesItem: ArticlesItem? get() = articlesResevedLiveData.value

}