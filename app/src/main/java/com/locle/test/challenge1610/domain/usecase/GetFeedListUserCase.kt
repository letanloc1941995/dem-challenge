package com.locle.test.challenge1610.domain.usecase

import com.locle.test.challenge1610.domain.model.FeedResponse
import com.locle.test.challenge1610.domain.repository.FeedListRepository
import com.locle.test.challenge1610.domain.usecase.base.SingleUserCase
import io.reactivex.Single
import javax.inject.Inject


class GetFeedListUserCase @Inject constructor(private val repository: FeedListRepository) :
    SingleUserCase<FeedResponse>() {
    override fun buildUseCaseSingle(): Single<FeedResponse> {
        return repository.invokeFeedList()
    }
}