package com.locle.test.challenge1610.data.repository

import com.locle.test.challenge1610.data.source.network.ApiInterface
import com.locle.test.challenge1610.domain.model.FeedResponse
import com.locle.test.challenge1610.domain.repository.FeedListRepository
import io.reactivex.Single

class FeedListRepositoryImp(private val apiInterface: ApiInterface) : FeedListRepository {
    override fun invokeFeedList(): Single<FeedResponse> {
        return apiInterface.invokeFeedList()
    }
}