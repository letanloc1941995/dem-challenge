package com.locle.test.challenge1610.presentation.feedlist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.locle.test.challenge1610.domain.model.ArticlesItem
import com.locle.test.challenge1610.domain.model.FeedResponse
import com.locle.test.challenge1610.domain.usecase.GetFeedListUserCase
import com.locle.test.challenge1610.presentation.base.BaseViewModel
import io.reactivex.Single
import javax.inject.Inject

class FeedListViewModel @Inject constructor(private val getListUseCase: GetFeedListUserCase) :
    BaseViewModel() {
    init {
        isLoad.value = false
    }

    val feedListReceivedLiveData: MutableLiveData<FeedResponse> by lazy { MutableLiveData<FeedResponse>() }

    fun invokeFeedListData() {
        getListUseCase.execute(
            onSuccess = {
                isLoad.value = true
                feedListReceivedLiveData.value = it
            },
            onError = {
                isLoad.value = true
                it.printStackTrace()
            }
        )
    }

}