package com.locle.test.challenge1610.presentation.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.locle.test.challenge1610.domain.model.FeedResponse

abstract class BaseViewModel : ViewModel() {
    val isLoad: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }
}