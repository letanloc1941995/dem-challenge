package com.locle.test.challenge1610.di.component

import android.app.Application
import com.locle.test.challenge1610.MyApp
import com.locle.test.challenge1610.di.module.ActivityModule
import com.locle.test.challenge1610.di.module.ApplicationModule
import com.locle.test.challenge1610.di.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ApplicationModule::class,
        ActivityModule::class,
        NetworkModule::class]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    fun inject(application: MyApp)
}