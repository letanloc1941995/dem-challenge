package com.locle.test.challenge1610.domain.model

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ArticlesItem(
    @Json(name = "image")
    val image: String? = null,

    @Json(name = "dc:creator")
    val dcCreator: String? = null,

    @Json(name = "description")
    val description: String? = null,

    @Json(name = "detail")
    val detail: String? = null,

    @Json(name = "title")
    val title: String? = null,

    @Json(name = "category")
    val category: List<String?>? = null,

    @Json(name = "pubDate")
    val pubDate: String? = null
) : Parcelable