package com.locle.test.challenge1610.presentation.feedlist

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.locle.test.challenge1610.R
import com.locle.test.challenge1610.domain.model.ArticlesItem
import com.locle.test.challenge1610.presentation.feedDetail.FeedDetailActivity
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_feed_detail.*
import kotlinx.android.synthetic.main.activity_feed_list.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.progress_bar
import javax.inject.Inject

class FeedListActivity : DaggerAppCompatActivity(), OnFeedAdapterListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var feedListAdapter: FeedListAdapter? = null

    private val FEED_DETAIL = 100

    private val viewModel: FeedListViewModel by lazy {
        ViewModelProvider(this, viewModelFactory)
            .get(FeedListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed_list)
        setupActionbar()
        initRecycleView()
        showProgressbar()
        initFeedData()
    }

    private fun setupActionbar() {
        setSupportActionBar(tool_bar)
    }

    private fun initRecycleView() {
        feedListAdapter = FeedListAdapter(this)
        val viewManager = LinearLayoutManager(this)
        recycle.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = feedListAdapter
        }
    }

    private fun initFeedData() {
        viewModel.feedListReceivedLiveData.observe(this, Observer {
            it?.let { feedListAdapter?.addData(it.articles) }
        })
        // call API
        viewModel.invokeFeedListData()
    }

    private fun showProgressbar() {
        viewModel.isLoad.observe(this,
            Observer {
                it?.let { isVisibility ->
                    progress_bar?.apply {
                        visibility = if (isVisibility) View.GONE else View.VISIBLE
                    }
                }
            })
    }

    private fun navigateToFeedDetail(data: ArticlesItem) {
        val intent = Intent(this, FeedDetailActivity::class.java)
        intent.putExtra("data", data)
        startActivityForResult(intent, FEED_DETAIL)
    }

    override fun onClick(data: ArticlesItem, pos: Int) {
        navigateToFeedDetail(data)
    }

    override fun onDestroy() {
        super.onDestroy()
        feedListAdapter = null;
    }

}
