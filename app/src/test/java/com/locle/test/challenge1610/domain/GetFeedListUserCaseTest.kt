package com.locle.test.challenge1610.domain

import com.locle.test.challenge1610.domain.model.ArticlesItem
import com.locle.test.challenge1610.domain.model.FeedResponse
import com.locle.test.challenge1610.domain.repository.FeedListRepository
import com.locle.test.challenge1610.domain.usecase.GetFeedListUserCase
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class GetFeedListUserCaseTest {
    private lateinit var repository: FeedListRepository
    private lateinit var userCase: GetFeedListUserCase

    @Before
    fun setup() {
        repository = mock()
        userCase = GetFeedListUserCase(repository)
    }

    @Test
    fun `should return  list when invoke success`() {
        whenever(repository.invokeFeedList()).thenReturn(Single.just(feedData))
        userCase.buildUseCaseSingle().test()
            .assertValue(feedData)
        verify(repository).invokeFeedList()
    }

    @Test
    fun `should return flight list when invoke failed`() {
        whenever(repository.invokeFeedList()).thenReturn(Single.error(error))

        userCase.buildUseCaseSingle().test()
            .assertError(error)

        verify(repository).invokeFeedList()
    }

    private companion object {
        val error = Throwable("Error message")
        val list = List(5) {
            ArticlesItem(title = "")
            ArticlesItem(title = "")
            ArticlesItem(title = "")
            ArticlesItem(title = "")
            ArticlesItem(title = "")
        }
        var feedData: FeedResponse = FeedResponse(list)
    }


}