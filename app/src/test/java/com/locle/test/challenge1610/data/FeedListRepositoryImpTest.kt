package com.locle.test.challenge1610.data

import com.locle.test.challenge1610.data.repository.FeedListRepositoryImp
import com.locle.test.challenge1610.data.source.network.ApiInterface
import com.locle.test.challenge1610.domain.model.ArticlesItem
import com.locle.test.challenge1610.domain.model.FeedResponse
import com.locle.test.challenge1610.domain.repository.FeedListRepository
import org.junit.Before
import org.junit.Test
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single


/// test api
class FeedListRepositoryImpTest {
    private lateinit var repository: FeedListRepository
    private lateinit var apiInterface: ApiInterface

    @Before
    fun setUp() {
        apiInterface = mock()
        repository = FeedListRepositoryImp(apiInterface)
    }

    @Test
    fun `should return flight list when invoke success`() {
        whenever(apiInterface.invokeFeedList()).thenReturn(Single.just(feedData))
        repository.invokeFeedList().test().assertValue(feedData)
        verify(apiInterface).invokeFeedList()
    }

    @Test
    fun `should return flight list when invoke failed`() {
        whenever(apiInterface.invokeFeedList()).thenReturn(Single.error(error))
        repository.invokeFeedList().test()
            .assertError(error)
        verify(apiInterface).invokeFeedList()
    }

    private companion object {
        val error = Throwable("Error message")
        val list = List(5) {
            ArticlesItem(title = "")
            ArticlesItem(title = "")
            ArticlesItem(title = "")
            ArticlesItem(title = "")
            ArticlesItem(title = "")
        }
        var feedData: FeedResponse = FeedResponse(list)
    }
}